from django.test import TestCase, Client
from django.urls import resolve
from status import views
from status.models import Status
import datetime
from jonathanjocan.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
import time
import os

c = Client()

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_ada_url_benar(self):
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_tidak_ada_url_slash_status(self):
        response = c.get('/status/')
        self.assertEqual(response.status_code, 404)

    def test_apakah_pake_fungsi_status(self):
        response = resolve('/')
        self.assertEqual(response.func, views.status)

    def test_apakah_pake_template_status_html(self):
        response = c.get('')
        self.assertTemplateUsed(response, 'status.html')

    def test_apakah_ada_tulisan_halo_apa_kabar(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("Halo, apa kabar?", content)

    def test_apakah_ada_input_form_status(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('<label>Isi Status:</label>', content)
        self.assertIn('<input type="text" name="status" maxlength="300"', content)

    def test_apakah_ada_button_dengan_tulisan_submit(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('<input name="submit_button" type="submit" value="Submit"', content)

    def test_apakah_model_bisa_buat_status_baru_atau_engga(self):
        new_status = Status.objects.create(status='lagi bosen')
        self.assertEqual(Status.objects.all().count(), 1)

    def test_apakah_bisa_menyimpan_POST_request(self):
        response = self.client.post('', data={'status':'bosen'})
        self.assertEqual(Status.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        response_lain = c.get('')
        content = response_lain.content.decode('utf8')
        self.assertIn('bosen', content)
        self.assertIn(datetime.datetime.now().strftime("%d %b %Y, %H:%M"), content)
        
    
class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FuncTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTest, self).tearDown()

    def test_apakah_formnya_bisa_apa_engga(self):
        self.selenium.get(self.live_server_url)

        temp_input = self.selenium.find_element_by_name("status")
        temp_input.send_keys('mau bobo')

        temp_button = self.selenium.find_element_by_name("submit_button")
        temp_button.submit()

        time.sleep(3)

        self.assertIn('mau bobo', self.selenium.page_source)

