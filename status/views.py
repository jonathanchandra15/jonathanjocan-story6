from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms, models

# Create your views here.
def status(request):
    model = models.Status.objects.all()
    if request.method == "POST":
        form = forms.StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('status:home')
    else:
        form = forms.StatusForm()
    return render(request, 'status.html', {'form':form, 'model':model})